import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class Stack implements StackOperations{

    private List<String> stackHolder = new ArrayList<>();
    private final int STACK_TOP = 0;

    @Override
    public List<String> get() {
        return stackHolder;
    }

    @Override
    public Optional<String> pop() {
        if(stackHolder.isEmpty()){
            return Optional.empty();
        }
        return Optional.of(stackHolder.remove(STACK_TOP));
    }

    @Override
    public void push(String item) {
        stackHolder.add(STACK_TOP,item);
    }
}
