public class Main {

    public static void main(String[] args) {
		Stack stack = new Stack();
        stack.push("work!");
        stack.push("stack");
        stack.push("a");
        stack.push("how");
		stack.push("That's");
		System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stack.pop());

    }
}
